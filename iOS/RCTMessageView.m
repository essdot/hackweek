//
//  RCTMessageView.m
//  HackWeek
//
//  Created by Justin Sippel on 4/23/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import "RCTMessageView.h"

#import "UAInbox.h"
#import "UAirship.h"
#import "UAInboxMessageViewController.h"

@implementation RCTMessageView
{
  UAInboxMessageViewController *mvc;
}
- (instancetype) init {
  self = [super init];

  mvc = [[UAInboxMessageViewController alloc] initWithNibName:@"UAInboxMessageViewController" bundle:nil];
  mvc.closeBlock = ^(BOOL animated){};
  [self addSubview:mvc.view];
  [mvc loadMessageAtIndex:self.messageIndex];
  
  return self;
}

- (void)setMessageIndex:(int)messageIndex
{
  [mvc loadMessageAtIndex:messageIndex];

  [self setNeedsDisplay];
}



@end
