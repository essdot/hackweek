//
//  RCTMessageViewManager.m
//  HackWeek
//
//  Created by Justin Sippel on 4/23/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import "RCTMessageViewManager.h"
#import "RCTMessageView.h"
#import "RCTBridge.h"
#import "UIView+React.h"
#import <UIKit/UIKit.h>


@implementation RCTMessageViewManager

RCT_EXPORT_MODULE();

RCT_EXPORT_VIEW_PROPERTY(messageIndex, int);

- (UIView *)view
{
//  UAInboxMessageViewController *mvc;
//  mvc = [[UAInboxMessageViewController alloc] initWithNibName:@"UAInboxMessageViewController" bundle:nil];
//  mvc.closeBlock = ^(BOOL animated){};
//  
//  [mvc loadMessageAtIndex:0];
//
//  return mvc.view;
  return [[RCTMessageView alloc] init];
}

@end
