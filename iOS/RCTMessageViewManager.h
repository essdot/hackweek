//
//  RCTMessageViewManager.h
//  HackWeek
//
//  Created by Justin Sippel on 4/23/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTViewManager.h"

@interface RCTMessageViewManager : RCTViewManager

@end
