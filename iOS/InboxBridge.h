//
//  InboxBridge.h
//  HackWeek
//
//  Created by Justin Sippel on 4/22/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import "RCTBridgeModule.h"

@interface InboxBridge : NSObject <RCTBridgeModule>
@end
