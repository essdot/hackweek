//
//  InboxBridge.m
//  HackWeek
//
//  Created by Justin Sippel on 4/22/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import "InboxBridge.h"

#import "RCTBridge.h"
#import "RCTBridgeModule.h"
#import "RCTEventDispatcher.h"

#import "UAInboxLocalization.h"
#import "UAInbox.h"
#import "UAInboxMessage.h"
#import "UAInboxMessageList.h"
#import "UAirship.h"
#import "UAInboxMessageViewController.h"


@implementation InboxBridge

RCT_EXPORT_MODULE();
@synthesize bridge = _bridge;
//
//- (instancetype)init {
//  NSLog(@"inbox bridge init");
//
//  self.messages =
//  return self;
//}

RCT_EXPORT_METHOD(doEmit){
  [self emitMessagesLoaded];
}

- (NSDictionary *) marshalMessage:(UAInboxMessage *)message {
  return @{
           @"title": message.title,
           @"url": message.messageBodyURL.absoluteString,
           @"id": message.messageID
  };
}

- (NSArray *) marshalMessages:(NSArray *)messages {
  NSMutableArray *returnArray = [NSMutableArray arrayWithCapacity:messages.count];
  
  for (UAInboxMessage* object in messages) {
    [returnArray addObject:[self marshalMessage:object]];
  }
  
  return [NSArray arrayWithArray:returnArray];
}


- (void) emitMessagesLoaded {
  NSArray *messages = [self marshalMessages:[UAirship inbox].messageList.messages];
  NSDictionary *eventDict = @{@"messages" :  messages};
  
  [self.bridge.eventDispatcher sendDeviceEventWithName:@"messages-loaded" body:eventDict];
}

RCT_EXPORT_METHOD(displayMessage:(NSString *)messageId){
  dispatch_async(dispatch_get_main_queue(), ^{
    UAInboxMessageViewController *mvc;
    mvc = [[UAInboxMessageViewController alloc] initWithNibName:@"UAInboxMessageViewController" bundle:nil];
    mvc.closeBlock = ^(BOOL animated){};
  
    [mvc loadMessageForID:messageId];
  });
}


@end
