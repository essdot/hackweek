//
//  RCTMessageView.h
//  HackWeek
//
//  Created by Justin Sippel on 4/23/15.
//  Copyright (c) 2015 sdot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAInboxMessageViewController.h"

@interface RCTMessageView : UIView
@property (nonatomic) int messageIndex;
@end
