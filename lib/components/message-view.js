var React = require('react-native')
var DeviceEE = require('RCTDeviceEventEmitter')
var NativeModules = require('NativeModules')

var createNativeClass = require('createReactIOSNativeComponentClass')

var MessageView = React.createClass({
  componentDidMount: function() {
    console.log('message view props')
    console.log(this.props)
  },
  render: function() {
    var NativeMessageView = createNativeClass({
        validAttributes: { messageIndex: this.props.messageIndex },
        uiViewClassName: 'RCTMessageView'
    })

    return <NativeMessageView messageIndex={this.props.messageIndex}/>
  }
})

module.exports = MessageView