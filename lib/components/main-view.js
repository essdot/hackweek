var React = require('react-native')
var InboxView = require('./inbox-view')

var {
  NavigatorIOS,
  Text,
  View,
  StyleSheet,
  TouchableHighlight
} = React;

var MainView = React.createClass({
  componentDidMount: function() {
    console.log('main props')
    console.log(this.props)

  },
  _onPress: function(rev) {
    var nextRoute = {
      title: 'Inbox',
      component: InboxView,
      nav: this.props.nav
    }

    this.props.navigator.push(nextRoute)
  },
  render: function() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.button}
          onPress={this._onPress}
          underlayColor='#FF2222'>
          <Text style={styles.buttonText}>Press</Text>
        </TouchableHighlight>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    marginTop: 65
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#DD0000',
    padding: 10,
    marginTop: 65
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold'
  }
})

// AppRegistry.registerComponent('MainView', () => MainView);
module.exports = MainView;