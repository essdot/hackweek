var React = require('react-native')
var DeviceEE = require('RCTDeviceEventEmitter')
var NativeModules = require('NativeModules')

var createReactIOSNativeComponentClass = require('createReactIOSNativeComponentClass')

var MessageView = require('./message-view')

function getInbox() { return NativeModules.InboxBridge }

var {
  ScrollView,
  ListView,
  NavigatorIOS,
  Text,
  View,
  StyleSheet,
  TouchableHighlight
} = React;


var InboxView = React.createClass({
  componentDidMount: function() {
    DeviceEE.addListener('messages-loaded', (data) => {
      console.log('messages-loaded')
      console.log(data.messages)
      this.setState({
          messages: data.messages.slice()
        , dataSource: this.state.dataSource.cloneWithRows(data.messages.slice())
      })
    })

    this.inbox = getInbox()
    this.inbox.doEmit()
  },
  render: function() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        style={styles.list} >
      </ListView>
    );
  },
  _onPress: function(message, index) {
    // if(messageIndex) {
    //   getInbox().displayMessage(messageIndex)
    // }
    console.log('click index ' + index)
    console.log('tupeof index ' + (typeof index))
    this.props.navigator.push({
      title: message.title,
      component: MessageView,
      passProps: {message: message, messageIndex: (index - 0)}
    })
  },
  renderRow: function(message, thing, index) {
    return (
      <TouchableHighlight
        onPress={() => this._onPress(message, index)}
        style={styles.rowButton}
        underlayColor='#C4CACC' >
        <Text style={styles.rowText}>{message.title}</Text>
      </TouchableHighlight>
    )
  },
  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    return {
        messages: []
      , dataSource: ds.cloneWithRows([])
    }
  }
})


var styles = StyleSheet.create({
    list: {
        paddingTop: 20
      , backgroundColor: '#F5FCFF'
    }
  , rowButton: {
      flexDirection: 'row'
    , paddingLeft: 10
    , paddingRight: 10
    , paddingTop: 20
    , paddingBottom: 20
    , borderWidth: 0.5
    , borderColor: '#CCCCCC'
  }
  , rowText: {
        fontSize: 25
      , fontWeight: 'bold'
  }
})

module.exports = InboxView;
