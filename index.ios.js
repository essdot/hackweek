/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var DeviceEE = require('RCTDeviceEventEmitter');

var MainView = require('./lib/components/main-view');
var InboxView = require('./lib/components/inbox-view');

var {
  ScrollView,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NavigatorIOS
} = React;

var styles = StyleSheet.create({
  mainNav: {
    flex: 1
  }
})

var HackWeek = React.createClass({
  componentDidMount: function() {
    
  },
  render: function() {
    return <NavigatorIOS
        style={styles.wrapper}
        initialRoute={{
          backButtonTitle: 'Back to Main',
          component: MainView,
          title: 'Hack Week'
        }} />
  }
});

var styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  wrapper: {
    flex: 1
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('HackWeek', () => HackWeek);


var routes = {
  inbox: {
    title: 'Inbox',
    component: InboxView,
    backButtonTitle: 'Back to main'
  }
}